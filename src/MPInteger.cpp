#include "MPInteger.h"
#include <climits>
#include <cstring>
#include <iomanip>
#include <regex>
#include <sstream>

typedef MPInteger::ElemType ElemType;

std::ostream &operator<<(std::ostream &os, const MPInteger &obj)
{
    constexpr auto base = std::ios_base::dec |
                          std::ios_base::hex |
                          std::ios_base::oct;

    const auto f = os.flags() & base;

    switch (f) {
    case std::ios_base::dec:
        obj.printDec(os);
        break;

    case std::ios_base::hex:
        obj.printHex(os);
        break;

    case std::ios_base::oct:
        obj.printOct(os);
        break;

    default:
        obj.printDec(os);
        break;
    }

    return os;
}

std::istream &operator>>(std::istream &is, MPInteger &obj)
{
    std::string input;
    is >> input;
    obj.parseString(input.c_str());
    return is;
}

bool MPInteger::isHexadecimalString(const char *str)
{
    static std::regex hex("(\\+|\\-)?0x[\\da-fA-F]+");
    return std::regex_match(str, hex);
}

bool MPInteger::isBinaryString(const char *str)
{
    static std::regex bin("(\\+|\\-)?0b[01]+");
    return std::regex_match(str, bin);
}

bool MPInteger::isDecimalString(const char *str)
{
    static std::regex dec("(\\+|\\-)?\\d+");
    return std::regex_match(str, dec);
}

bool MPInteger::isOctalString(const char *str)
{
    static std::regex oct("(\\+|\\-)?0o[0-7]+");
    return std::regex_match(str, oct);
}

MPInteger::MPInteger():
    bits_(0), initialized_(false), isNegative_(false)
{

}

MPInteger::MPInteger(const void *src, const unsigned long long bits)
{
    if (src && bits) {
        assign(src, bits);
    } else {
        bits_ = 0;
        initialized_ = false;
        isNegative_ = false;
    }
}

MPInteger::MPInteger(const MPInteger &src,
                     const unsigned long long pos,
                     const unsigned long long len)
{
    if (src.initialized_ &&
        pos < src.bits_ &&
        pos + len <= src.bits_) {
        assign(src, pos, len);
    } else {
        bits_ = 0;
        initialized_ = false;
        isNegative_ = false;
    }
}

MPInteger::MPInteger(const char *str)
{
    if (!parseString(str)) {
        bits_ = 0;
        initialized_ = false;
        isNegative_ = false;
    }
}

void MPInteger::assign(const void *src, const unsigned long long bits)
{
    if (src && bits) {
        const auto rem = bits % ELEM_BITNUM_;
        const auto num = bits / ELEM_BITNUM_;

        const ElemType *esrc = static_cast<const ElemType*>(src);
        mem_.assign(esrc, esrc + num);

        if (rem) {
            const unsigned char *csrc = static_cast<const unsigned char*>(src);
            const unsigned char *ptr = csrc + (num * ELEM_SIZE_);

            // bytes = rem / 8 + ((rem % 8) ? 1 : 0);
            const auto bytes = (rem >> 3) + ((rem & 7) ? 1 : 0);
            ElemType val = 0;
            for (int i = 0; i < bytes; ++i) {
                val |= (*ptr) << (i * 8);
                ++ptr;
            }

            // set extra bits to 0
            val &= (static_cast<ElemType>(1) << rem) - 1;

            // write to mem_
            mem_.push_back(val);
        }

        initialized_ = true;
        isNegative_ = false;
        countBits_();
        shrink();
    }
}

void MPInteger::assign(const MPInteger &src,
                       const unsigned long long pos,
                       const unsigned long long len)
{
    if (src.initialized_) {
        if (pos >= src.bits_ || pos + len > src.bits_) {
            return;
        }

        if (len == 0) {
            toZero();
        } else {
            const auto rem = len % ELEM_BITNUM_;
            const auto num = len / ELEM_BITNUM_;

            mem_.resize(num + 1);

            if (rem) {
                mem_[num] = src.fetchBits(pos + num * ELEM_BITNUM_) &
                            ((static_cast<ElemType>(1) << rem) - 1);
            }
            unsigned long long offset = pos + (num - 1) * ELEM_BITNUM_;
            for (long long i = num - 1; i >= 0; --i) {
                mem_[i] = src.fetchBits(offset);
                offset -= ELEM_BITNUM_;
            }

            initialized_ = true;
            countBits_();
            shrink();
            // set sign
            isNegative_ = false;
        }
    }
}

void MPInteger::assign(const long long val)
{
    constexpr auto LLONG_BITNUM = sizeof(val) << 3;
    if (val >= 0) {
        assign(&val, LLONG_BITNUM);
    } else {
        if (val == LLONG_MIN) {
            constexpr auto MOD = 1ULL << (LLONG_BITNUM - 1);
            assign(&MOD, LLONG_BITNUM);
        } else {
            const auto tmp = -val;
            assign(&tmp, LLONG_BITNUM);
        }
        // set sign
        isNegative_ = true;
    }
}

bool MPInteger::parseHex(const char *str)
{
    if (str && isHexadecimalString(str)) {
        if (str[0] == '+') {
            parseHex(str + 1);
        } else if (str[0] == '-') {
            parseHex(str + 1);
            toNegative();
        } else {
            const auto len = strlen(str);
            // reserve_size = (len * 4) / ELEM_BITNUM_ + 1;
            const auto reserve_size = (len << 2) / ELEM_BITNUM_ + 1;

            mem_.clear();
            mem_.resize(reserve_size);

            unsigned long long num = 0;
            unsigned long long bit = 0;
            for (long long i = len - 1; i >= 2; --i) {
                const unsigned int hex = isdigit(str[i]) ? (str[i] - '0') :
                                                           (tolower(str[i]) - 'a' + 10);
                for (int j = 0; j < 4; ++j) {
                    mem_[num] |= (hex & (1U << j)) ? (static_cast<ElemType>(1) << bit) : 0;
                    ++bit;
                    if (bit == ELEM_BITNUM_) {
                        bit = 0;
                        ++num;
                    }
                }
            }

            initialized_ = true;
            countBits_();
            shrink();
        }

        return true;
    }

    return false;
}

bool MPInteger::parseBin(const char *str)
{
    if (str && isBinaryString(str)) {
        if (str[0] == '+') {
            parseBin(str + 1);
        } else if (str[0] == '-') {
            parseBin(str + 1);
            toNegative();
        } else {
            const auto len = strlen(str);
            // reserve_size = len / 8 + 1;
            const auto reserve_size = (len >> 3) + 1;

            mem_.clear();
            mem_.resize(reserve_size);

            unsigned long long num = 0;
            unsigned long long bit = 0;
            for (long long i = len - 1; i >= 2; --i) {
                mem_[num] |= (str[i] != '0') ? (static_cast<ElemType>(1) << bit) : 0;
                ++bit;
                if (bit == ELEM_BITNUM_) {
                    bit = 0;
                    ++num;
                }
            }

            initialized_ = true;
            countBits_();
            shrink();
        }

        return true;
    }

    return false;
}

bool MPInteger::parseDec(const char *str)
{
    if (str && isDecimalString(str)) {
        if (str[0] == '+') {
            parseDec(str + 1);
        } else if (str[0] == '-') {
            parseDec(str + 1);
            toNegative();
        } else {
            const auto len = strlen(str);

            // let *this = 0
            toZero();

            // let weight = 1
            MPInteger weight;
            weight.mem_.push_back(1);
            weight.bits_ = 1;
            weight.initialized_ = true;

            MPInteger digit(str[len - 1] - '0');
            // *this += digit
            additionAssignment(digit);
            for (long long i = len - 2; i >= 0; --i) {
                digit.assign(str[i] - '0');
                // weight *= 10
                weight.multiplicationAssignment(getTen());
                // *this += digit * weight
                additionAssignment(digit.multiplication(weight));
            }
        }

        return true;
    }

    return false;
}

bool MPInteger::parseOct(const char *str)
{
    if (str && isOctalString(str)) {
        if (str[0] == '+') {
            parseOct(str + 1);
        } else if (str[0] == '-') {
            parseOct(str + 1);
            toNegative();
        } else {
            const auto len = strlen(str);
            const auto reserve_size = (len * 3) / ELEM_BITNUM_ + 1;

            mem_.clear();
            mem_.resize(reserve_size);

            unsigned long long num = 0;
            unsigned long long bit = 0;
            for (long long i = len - 1; i >= 2; --i) {
                const unsigned int oct = str[i] - '0';
                for (int j = 0; j < 3; ++j) {
                    mem_[num] |=
                        (oct & (1U << j)) ? (static_cast<ElemType>(1) << bit) : 0;
                    ++bit;
                    if (bit == ELEM_BITNUM_) {
                        bit = 0;
                        ++num;
                    }
                }
            }

            initialized_ = true;
            countBits_();
            shrink();
        }

        return true;
    }

    return false;
}

void MPInteger::printHex(std::ostream &os) const
{
    constexpr auto WIDTH = ELEM_BITNUM_ / 4;

    if (initialized_) {
        if (bits_ == 0) {
            os << "0x0";
        } else {
            // store the original state of os
            std::ios os_bak(nullptr);
            os_bak.copyfmt(os);

            if (isNegative_) {
                os << '-';
            }

            os << "0x";
            const auto num = bits_ / ELEM_BITNUM_;
            const auto rem = bits_ % ELEM_BITNUM_;

            if (rem) {
                const auto h = mem_[num] & ((static_cast<ElemType>(1) << rem) - 1);
                os << std::hex << h;
            }

            for (long long i = num - 1; i >= 0; --i) {
                os << std::hex << std::setfill('0') << std::setw(WIDTH) << mem_[i];
            }

            // restore the state of os
            os.copyfmt(os_bak);
        }
    }
}

void MPInteger::printBin(std::ostream &os) const
{
    if (initialized_) {
        if (bits_ == 0) {
            os << "0b0";
        } else {
            if (isNegative_) {
                os << '-';
            }

            os << "0b";
            const auto num = bits_ / ELEM_BITNUM_;
            const auto rem = bits_ % ELEM_BITNUM_;

            if (rem) {
                // find the location of the first none-zero bit
                int i = ELEM_BITNUM_ - 1;
                for (; i >= 0; --i) {
                    if (mem_[num] & (static_cast<ElemType>(1) << i)) {
                        break;
                    }
                }

                for (; i >= 0; --i) {
                    os << ((mem_[num] & (static_cast<ElemType>(1) << i)) ? '1' : '0');
                }
            }

            for (long long i = num - 1; i >= 0; --i) {
                for (int j = ELEM_BITNUM_ - 1; j >= 0; --j) {
                    os << ((mem_[i] & (static_cast<ElemType>(1) << j)) ? '1' : '0');
                }
            }
        }
    }
}

void MPInteger::printDec(std::ostream &os) const
{
    if (initialized_) {
        if (bits_ == 0) {
            os << '0';
        } else {
            std::ostringstream oss;
            printHex(oss);
            std::vector<unsigned char> dec;
            dec.reserve(oss.str().length());

            // print sign
            if (isNegative_) {
                os << '-';
            }

            MPInteger tmp(*this);
            tmp.isNegative_ = false;
            // while tmp > 0
            while (tmp.bits_) {
                MPInteger digit(tmp.modulo(getTen()));
                dec.push_back(digit.bits_ == 0 ? 0 : digit.mem_[0]);
                tmp.divisionAssignment(getTen());
            }

            for (auto iter = dec.crbegin(); iter != dec.crend(); ++iter) {
                os << static_cast<unsigned int>(*iter);
            }
        }
    }
}

void MPInteger::printOct(std::ostream &os) const
{
    if (initialized_) {
        if (bits_ == 0) {
            os << "0o0";
        } else {
            // store the original state of os
            std::ios os_bak(nullptr);
            os_bak.copyfmt(os);

            if (isNegative_) {
                os << '-';
            }

            os << "0o";

            const auto num = bits_ / 3;
            const auto rem = bits_ % 3;

            if (rem) {
                os << std::oct << (fetchBits(bits_ - rem) & 7);
            }

            for (long long pos = (num - 1) * 3; pos >= 0; pos -= 3) {
                os << std::oct << (fetchBits(pos) & 7);
            }

            // restore the state of os
            os.copyfmt(os_bak);
        }
    }
}

unsigned long long MPInteger::countBits_()
{
    if (initialized_) {
        for (long long i = mem_.size() - 1; i >= 0; --i) {
            // find the first none-zero element
            if (mem_[i]) {
                // count the number of bits, ignore leading 0s
                int cnt = 0;
                for (int j = ELEM_BITNUM_ - 1; j >= 0; --j) {
                    if (mem_[i] & (static_cast<ElemType>(1) << j)) {
                        cnt = j + 1;
                        break;
                    }
                }

                bits_ = cnt + i * ELEM_BITNUM_;
                return bits_;
            }
        }

        bits_ = 0;
    }

    return bits_;
}

int MPInteger::bitAt(const unsigned long long pos) const
{
    if (bits_ == 0) {
        return 0;
    } else if (pos < bits_) {
        const auto num = pos / ELEM_BITNUM_;
        const auto rem = pos % ELEM_BITNUM_;

        return (mem_[num] & (static_cast<ElemType>(1) << rem)) ? 1 : 0;
    }

    return -1;
}

/**
 * Example:
 *
 * mem_[0] = 0b10010110
 * mem_[1] = 0b11010011
 *
 *                 MSB                  LSB
 *                  --------------------->
 *                  mem_[1]   , mem[0]
 * -----------------------------------------------------
 * fetchBits( 0) == 0b11010011, 0b10010110 == 0b10010110
 *                                ^^^^^^^^
 * fetchBits( 1) == 0b11010011, 0b10010110 == 0b11001011
 *                           ^    ^^^^^^^
 * fetchBits( 4) == 0b11010011, 0b10010110 == 0b00111001
 *                        ^^^^    ^^^^
 * fetchBits(13) == 0b11010011, 0b10010110 == 0b110
 *                    ^^^
 * fetchBits(17) == 0 // out of range
 * -----------------------------------------------------
 */
ElemType MPInteger::fetchBits(const unsigned long long pos) const
{
    if (pos >= bits_) {
        return 0;
    }

    ElemType ret = 0;

    const auto num = pos / ELEM_BITNUM_;
    const auto rem = pos % ELEM_BITNUM_;

    if (rem == 0) {
        ret = mem_[num];
    } else {
        const auto ret_bitnum =
            std::min(bits_ - pos, static_cast<unsigned long long>(ELEM_BITNUM_));
        const auto end = rem + ret_bitnum;
        if (end > ELEM_BITNUM_) {
            const auto low_bitnum = ELEM_BITNUM_ - rem;
            const auto high_bitnum = rem;

            const auto low_mask = ~((static_cast<ElemType>(1) << rem) - 1);
            const auto high_mask = (static_cast<ElemType>(1) << rem) - 1;

            ret = ((mem_[num] & low_mask) >> high_bitnum) |
                  ((mem_[num + 1] & high_mask) << low_bitnum);
        } else {
            const auto mask = (end == ELEM_BITNUM_) ?
                                  ELEM_MAX_VALUE_ :
                                  ((static_cast<ElemType>(1) << end) - 1);

            ret = (mem_[num] & mask) >> rem;
        }
    }

    return ret;
}

bool MPInteger::equalTo(const MPInteger &rhs) const
{
    if (!initialized_ || !rhs.initialized_) {
        return false;
    }

    if (this == &rhs) {
        return true;
    }

    if (isNegative_ != rhs.isNegative_) {
        return false;
    }

    if (bits_ != rhs.bits_) {
        return false;
    }

    const auto rem = bits_ % ELEM_BITNUM_;
    const auto num = bits_ / ELEM_BITNUM_ + (rem ? 1 : 0);
    for (unsigned long long i = 0; i < num; ++i) {
        if (mem_[i] != rhs.mem_[i]) {
            return false;
        }
    }

    return true;
}

bool MPInteger::lessThan(const MPInteger &rhs) const
{
    if (!initialized_ || !rhs.initialized_) {
        return false;
    }

    if (equalTo(rhs)) {
        return false;
    }

    if (isNegative_ && !rhs.isNegative_) {
        // lhs < 0, rhs >= 0
        return true;
    } else if (!isNegative_ && rhs.isNegative_) {
        // lhs >= 0, rhs < 0
        return false;
    } else {
        bool absLessThan = false;

        if (bits_ != rhs.bits_) {
            absLessThan = bits_ < rhs.bits_;
        } else {
            const auto rem = bits_ % ELEM_BITNUM_;
            const auto num = bits_ / ELEM_BITNUM_ + (rem ? 1 : 0);
            for (long long i = num - 1; i >= 0; --i) {
                if (mem_[i] != rhs.mem_[i]) {
                    absLessThan = mem_[i] < rhs.mem_[i];
                    break;
                }
            }
        }

        return isNegative_ ? (!absLessThan) : absLessThan;
    }
}

MPInteger &MPInteger::additionAssignment(const MPInteger &rhs)
{
    if (!initialized_ || !rhs.initialized_ || rhs.bits_ == 0) {
        return *this;
    }

    if (bits_ == 0) {
        copy(rhs);
    } else if (absCompare(rhs) == 0) {
        if (isNegative_ != rhs.isNegative_) {
            // set to 0
            toZero();
        } else {
            bitwiseLeftShiftAssignment(1);
        }
    } else if (isNegative_ == rhs.isNegative_) {
        // lhs and rhs have the same sign

        const auto reserve_bits = std::max(bits_, rhs.bits_) + 1;
        const auto reserve_rem = reserve_bits % ELEM_BITNUM_;
        const auto reserve_size = reserve_bits / ELEM_BITNUM_ + (reserve_rem ? 1 : 0);

        mem_.resize(reserve_size);

        const auto sz = std::min(mem_.size(), rhs.mem_.size());
        unsigned long long buf;
        for (long long i = 0; i < sz; ++i) {
            buf = static_cast<unsigned long long>(mem_[i]) +
                  static_cast<unsigned long long>(rhs.mem_[i]);
            if (buf > ELEM_MAX_VALUE_) {
                mem_[i] = buf & ELEM_MAX_VALUE_;
                buf >>= ELEM_BITNUM_;

                long long pos = i + 1;
                while (buf) {
                    buf += mem_[pos];
                    if (buf > ELEM_MAX_VALUE_) {
                        mem_[pos] = buf & ELEM_MAX_VALUE_;
                        buf >>= ELEM_BITNUM_;
                        ++pos;
                    } else {
                        mem_[pos] = buf;
                        break;
                    }
                }
            } else {
                mem_[i] = buf;
            }
        }

        countBits_();
        shrink();
    } else {
        // opposite sign
        if (isNegative_) {
            // (-lhs) + rhs
            toOppositeSign();
            subtractionAssignment(rhs);
            toOppositeSign();
        } else {
            // lhs + (-rhs);
            rhs.toOppositeSign();
            subtractionAssignment(rhs);
            rhs.toOppositeSign();
        }
    }

    return *this;
}

MPInteger &MPInteger::subtractionAssignment(const MPInteger &rhs)
{
    if (!initialized_ || !rhs.initialized_ || rhs.bits_ == 0) {
        return *this;
    }

    if (bits_ == 0) {
        toOppositeSign();
    } else if (absCompare(rhs) == 0) {
        if (isNegative_ != rhs.isNegative_) {
            bitwiseLeftShiftAssignment(1);
        } else {
            // set to 0
            toZero();
        }
    } else if (isNegative_ == rhs.isNegative_) {
        // lhs and rhs have the same sign

        if (absCompare(rhs) == 1) {
            // abs(lhs) > abs(rhs)

            const auto reserve_bits = std::max(bits_, rhs.bits_);
            const auto reserve_rem = reserve_bits % ELEM_BITNUM_;
            const auto reserve_size = reserve_bits / ELEM_BITNUM_ + (reserve_rem ? 1 : 0);

            mem_.resize(reserve_size);

            const auto sz = std::min(mem_.size(), rhs.mem_.size());
            long long buf;
            for (long long i = 0; i < sz; ++i) {
                buf = static_cast<long long>(mem_[i]) - static_cast<long long>(rhs.mem_[i]);
                if (buf < 0) {
                    mem_[i] = buf + (1ULL << ELEM_BITNUM_);
                    buf = -1;

                    long long pos = i + 1;
                    while (buf < 0) {
                        buf += static_cast<long long>(mem_[pos]);
                        if (buf < 0) {
                            mem_[pos] = buf + (1ULL << ELEM_BITNUM_);
                            buf = -1;
                            ++pos;
                        } else {
                            mem_[pos] = buf;
                            break;
                        }
                    }
                } else {
                    mem_[i] = buf;
                }
            }

            countBits_();
            shrink();
        } else {
            // abs(lhs) < abs(rhs)
            MPInteger rhs_copy(rhs);
            rhs_copy.subtractionAssignment(*this);
            copy(rhs_copy);
            toOppositeSign();
        }
    } else {
        // opposite sign
        if (isNegative_) {
            // (-lhs) - rhs
            toOppositeSign();
            additionAssignment(rhs);
            toOppositeSign();
        } else {
            // lhs - (-rhs)
            rhs.toOppositeSign();
            additionAssignment(rhs);
            rhs.toOppositeSign();
        }
    }

    return *this;
}

MPInteger &MPInteger::multiplicationAssignment(const MPInteger &rhs)
{
    if (!initialized_ || !rhs.initialized_ || bits_ == 0) {
        return *this;
    }

    MPInteger ans = mul_karatsuba(*this, rhs);

    // set sign
    ans.isNegative_ = isNegative_ != rhs.isNegative_;

    copy(ans);
    return *this;
}

MPInteger &MPInteger::divisionAssignment(const MPInteger &rhs)
{
    // NOTE: The quotient is truncated towards zero (fractional part is discarded).
    if (!initialized_ || !rhs.initialized_ || bits_ == 0 || rhs.bits_ == 0) {
        return *this;
    }
    if (rhs.bits_ == 1) {
        // set sign
        isNegative_ = isNegative_ != rhs.isNegative_;
        return *this;
    }

    const auto cmp = absCompare(rhs);
    if (cmp == -1) {
        toZero();
    } else if (cmp == 0) {
        mem_.clear();
        mem_.push_back(1);
        bits_ = 1;
        initialized_ = true;
        isNegative_ = isNegative_ != rhs.isNegative_;
    } else {
        MPInteger ans;
        ans.mem_.resize(mem_.size());
        ans.initialized_ = true;
        ans.isNegative_ = false;

        // store the original signs of lhs and rhs
        const auto lhs_isnegative = isNegative_;
        const auto rhs_isnegative = rhs.isNegative_;

        // let lhs and rhs be unsigned temporarily
        isNegative_ = false;
        rhs.isNegative_ = false;

        // while lhs >= rhs
        while (greaterThanOrEqualTo(rhs)) {
            auto bit_diff = bits_ - rhs.bits_;
            MPInteger rhs_copy(rhs);
            rhs_copy.bitwiseLeftShiftAssignment(bit_diff);

            if (lessThan(rhs_copy)) {
                rhs_copy.bitwiseRightShiftAssignment(1);
                --bit_diff;
            }

            subtractionAssignment(rhs_copy);

            const auto num = bit_diff / ELEM_BITNUM_;
            const auto rem = bit_diff % ELEM_BITNUM_;
            ans.mem_[num] |= static_cast<ElemType>(1) << rem;
        }

        ans.countBits_();
        ans.shrink();
        copy(ans);

        // set the sign of lhs
        isNegative_ = lhs_isnegative != rhs_isnegative;
        // restore the sign of rhs
        rhs.isNegative_ = rhs_isnegative;
    }

    return *this;
}

MPInteger &MPInteger::moduloAssignment(const MPInteger &rhs)
{
    // (a/b)*b + a%b == a
    if (!initialized_ || !rhs.initialized_ || bits_ == 0 || rhs.bits_ == 0) {
        return *this;
    }
    if (rhs.bits_ == 1) {
        toZero();
        return *this;
    }
    if (bits_ == 1) {
        return *this;
    }

    const auto cmp = absCompare(rhs);
    // if cmp == -1, no operation needed
    if (cmp == 0) {
        toZero();
    } else if (cmp == 1) {
        // store the original signs of lhs and rhs
        const auto lhs_isnegative = isNegative_;
        const auto rhs_isnegative = rhs.isNegative_;

        // let lhs and rhs be unsigned temporarily
        isNegative_ = false;
        rhs.isNegative_ = false;

        while (greaterThanOrEqualTo(rhs)) {
            auto bit_diff = bits_ - rhs.bits_;
            MPInteger rhs_copy(rhs);
            rhs_copy.bitwiseLeftShiftAssignment(bit_diff);

            if (lessThan(rhs_copy)) {
                rhs_copy.bitwiseRightShiftAssignment(1);
                --bit_diff;
            }

            subtractionAssignment(rhs_copy);
        }

        // set the sign of lhs
        isNegative_ = lhs_isnegative;
        // restore the sign of rhs
        rhs.isNegative_ = rhs_isnegative;
    }

    return *this;
}

MPInteger &MPInteger::bitwiseLeftShiftAssignment(const unsigned long long n)
{
    if (initialized_ && bits_ && n) {
        const auto num = (bits_ + n) / ELEM_BITNUM_;
        const auto rem = (bits_ + n) % ELEM_BITNUM_;

        mem_.resize(num + (rem ? 1 : 0));

        long long pos;
        if (rem) {
            pos = bits_ - rem;
            mem_[num] = (pos >= 0) ? fetchBits(pos) :
                                     (fetchBits(0) << (-pos));
        }

        long long i = num - 1;
        pos = i * ELEM_BITNUM_ - n;
        for (; i >= 0 && pos + static_cast<long long>(ELEM_BITNUM_) > 0; --i, pos -= ELEM_BITNUM_) {
            mem_[i] = (pos >= 0) ? fetchBits(pos) :
                                   (fetchBits(0) << (-pos));
        }
        for (; i >= 0; --i) {
            mem_[i] = 0;
        }

        bits_ += n;
    }

    return *this;
}

MPInteger &MPInteger::bitwiseRightShiftAssignment(const unsigned long long n)
{
    if (initialized_ && bits_ && n) {
        if (n >= bits_) {
            // set to 0
            toZero();
        } else {
            const auto num = (bits_ - n) / ELEM_BITNUM_;
            const auto rem = (bits_ - n) % ELEM_BITNUM_;

            long long pos = n;
            for (long long i = 0; i < num; ++i) {
                mem_[i] = fetchBits(pos);
                pos += ELEM_BITNUM_;
            }

            if (rem) {
                mem_[num] = fetchBits(pos);
            }

            mem_.resize(num + (rem ? 1 : 0));

            bits_ -= n;
        }
    }

    return *this;
}

MPInteger MPInteger::multiplication(const MPInteger &rhs) const
{
    if (!initialized_ || !rhs.initialized_ || bits_ == 0) {
        return *this;
    }
    if (rhs.bits_ == 0) {
        return rhs;
    }

    MPInteger ans = mul_karatsuba(*this, rhs);

    // set sign
    ans.isNegative_ = isNegative_ != rhs.isNegative_;

    return ans;
}

MPInteger &MPInteger::preIncrement()
{
    if (initialized_) {
        if (bits_) {
            if (isNegative_) {
                toNotNegative();
                preDecrement();
                toNegative();
            } else if (mem_[0] != ELEM_MAX_VALUE_) {
                ++mem_[0];
                countBits_();
            } else {
                const auto reserve_bits = bits_ + 1;
                const auto reserve_rem = reserve_bits % ELEM_BITNUM_;
                const auto reserve_size = reserve_bits / ELEM_BITNUM_ + (reserve_rem ? 1 : 0);

                mem_.resize(reserve_size);

                for (auto &elem : mem_) {
                    if (elem == ELEM_MAX_VALUE_) {
                        elem = 0;
                    } else {
                        ++elem;
                        break;
                    }
                }

                countBits_();
                shrink();
            }
        } else {
            mem_.push_back(1);
            bits_ = 1;
        }
    }

    return *this;
}

MPInteger &MPInteger::preDecrement()
{
    if (initialized_) {
        if (bits_) {
            if (isNegative_) {
                toNotNegative();
                preIncrement();
                toNegative();
            } else if (mem_[0]) {
                --mem_[0];
                countBits_();
                shrink();
            } else {
                for (auto &elem : mem_) {
                    if (elem == 0) {
                        elem = ELEM_MAX_VALUE_;
                    } else {
                        --elem;
                        break;
                    }
                }

                countBits_();
                shrink();
            }
        } else {
            mem_.push_back(1);
            bits_ = 1;
            toNegative();
        }
    }

    return *this;
}

int MPInteger::absCompare(const MPInteger &rhs) const
{
    constexpr int err = 1 << (sizeof(int) * 8 - 1);

    if (!initialized_ || !rhs.initialized_) {
        return err;
    }

    if (this == &rhs) {
        return 0;
    }

    if (bits_ != rhs.bits_) {
        return (bits_ < rhs.bits_) ? (-1) : 1;
    }

    // else if bits_ == rhs.bits_
    const auto rem = bits_ % ELEM_BITNUM_;
    const auto num = bits_ / ELEM_BITNUM_ + (rem ? 1 : 0);

    bool absEqual = true;
    bool absLessThan = false;
    for (long long i = num - 1; i >= 0; --i) {
        if (mem_[i] != rhs.mem_[i]) {
            absEqual = false;
            absLessThan = mem_[i] < rhs.mem_[i];
            break;
        }
    }
    if (absEqual) {
        return 0;
    }

    return absLessThan ? (-1) : 1;
}

// private member functions
MPInteger MPInteger::mul(const MPInteger &a, const MPInteger &b)
{
    // NOTE: this function only calculates the product of the absolute values of the two operands
    // which means that the return value is always unsigned.
    MPInteger ans;

    if (!a.initialized_ || !b.initialized_) {
        return ans;
    }

    if (a.bits_ == 0 || b.bits_ == 0) {
        ans.toZero();
        return ans;
    }

    if (a.bits_ == 1) {
        ans.copy(b);
        ans.isNegative_ = false;
        return ans;
    }
    if (b.bits_ == 1) {
        ans.copy(a);
        ans.isNegative_ = false;
        return ans;
    }

    ans.toZero();

    MPInteger a_copy(a);
    a_copy.isNegative_ = false;
    unsigned long long a_shift = 0;

    for (unsigned long long i = 0; i < b.bits_; ++i) {
        if (b.bitAt(i)) {
            ans.additionAssignment(a_copy.bitwiseLeftShiftAssignment(i - a_shift));
            a_shift = i;
        }
    }

    return ans;
}

MPInteger MPInteger::mul_karatsuba(const MPInteger &a, const MPInteger &b)
{
    // NOTE: this function only calculates the product of the absolute values of the two operands
    // which means that the return value is always unsigned.
    MPInteger ans;

    if (!a.initialized_ || !b.initialized_) {
        return ans;
    }

    constexpr auto bits_threshold = 256ULL;
    if (a.bits_ < bits_threshold || b.bits_ < bits_threshold) {
        return mul(a, b);
    }

    const auto min_bits = std::min(a.bits_, b.bits_);
    // low_bits = min_bits / 2
    const auto low_bits = min_bits >> 1;
    const auto high_bits_a = a.bits_ - low_bits;
    const auto high_bits_b = b.bits_ - low_bits;

    MPInteger al, ah, bl, bh;

    // lower part of a
    al.assign(a, 0, low_bits);
    // higher part of a
    ah.assign(a, low_bits, high_bits_a);
    // lower part of b
    bl.assign(b, 0, low_bits);
    // higher part of b
    bh.assign(b, low_bits, high_bits_b);

    MPInteger pl = mul_karatsuba(al, bl);
    MPInteger ph = mul_karatsuba(ah, bh);
    MPInteger pm = mul_karatsuba(al.addition(ah), bl.addition(bh));

    pm.subtractionAssignment(pl);
    pm.subtractionAssignment(ph);
    pm.bitwiseLeftShiftAssignment(low_bits);

    // ph <<= (low_bits * 2)
    ph.bitwiseLeftShiftAssignment(low_bits << 1);

    ph.additionAssignment(pm);
    ph.additionAssignment(pl);

    // return ph + pm + pl
    return ph;
}

const MPInteger &MPInteger::getTen()
{
    static MPInteger instance;
    static bool constructed = false;

    if (!constructed) {
        instance.mem_.push_back(10);
        instance.bits_ = 4;
        instance.initialized_ = true;

        constructed = true;
    }

    return instance;
}
