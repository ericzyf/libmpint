#ifndef _MPINTEGER_H
#define _MPINTEGER_H

#include <iostream>
#include <vector>

class MPInteger {
    friend std::ostream &operator<<(std::ostream &os, const MPInteger &obj);
    friend std::istream &operator>>(std::istream &is, MPInteger &obj);

public:
    typedef unsigned int ElemType;
    static constexpr size_t ELEM_SIZE_ = sizeof(ElemType);
    static constexpr size_t ELEM_BITNUM_ = sizeof(ElemType) * 8;
    static constexpr ElemType ELEM_MAX_VALUE_ = ~static_cast<ElemType>(0);

    static bool isHexadecimalString(const char *str);
    static bool isBinaryString(const char *str);
    static bool isDecimalString(const char *str);
    static bool isOctalString(const char *str);

    MPInteger();
    MPInteger(const void *src, const unsigned long long bits);
    MPInteger(const MPInteger &src,
              const unsigned long long pos,
              const unsigned long long len);
    MPInteger(const long long val)
    {
        assign(val);
    }
    MPInteger(const char *str);
    void assign(const void *src, const unsigned long long bits);
    void assign(const MPInteger &src,
                const unsigned long long pos,
                const unsigned long long len);
    void assign(const long long val);
    bool parseHex(const char *str);
    bool parseBin(const char *str);
    bool parseDec(const char *str);
    bool parseOct(const char *str);
    bool parseString(const char *str)
    {
        return str ? (parseDec(str) || parseHex(str) || parseBin(str) || parseOct(str)) : false;
    }
    void printHex(std::ostream &os = std::cout) const;
    void printBin(std::ostream &os = std::cout) const;
    void printDec(std::ostream &os = std::cout) const;
    void printOct(std::ostream &os = std::cout) const;
    unsigned long long countBits_();
    int bitAt(const unsigned long long pos) const;
    // fetch ELEM_BITNUM_ bits starting at pos
    ElemType fetchBits(const unsigned long long pos) const;

    // reduce the size of mem_ according to bits_
    void shrink()
    {
        if (initialized_) {
            const auto num = bits_ / ELEM_BITNUM_;
            const auto rem = bits_ % ELEM_BITNUM_;

            const auto new_sz = num + (rem ? 1 : 0);
            if (new_sz != mem_.size()) {
                mem_.resize(new_sz);
            }
        }
    }

    // copy assignment operator
    // operator=
    void copy(const MPInteger &src)
    {
        mem_ = src.mem_;
        bits_ = src.bits_;
        initialized_ = src.initialized_;
        isNegative_ = src.isNegative_;
    }
    MPInteger &operator=(const MPInteger &rhs)
    {
        copy(rhs);
        return *this;
    }

    ///////////////////////
    // comparison operators
    // operator==
    bool equalTo(const MPInteger &rhs) const;
    bool operator==(const MPInteger &rhs) const
    {
        return equalTo(rhs);
    }

    // operator!=
    bool notEqualTo(const MPInteger &rhs) const
    {
        return !equalTo(rhs);
    }
    bool operator!=(const MPInteger &rhs) const
    {
        return notEqualTo(rhs);
    }

    // operator<
    bool lessThan(const MPInteger &rhs) const;
    bool operator<(const MPInteger &rhs) const
    {
        return lessThan(rhs);
    }

    // operator<=
    bool lessThanOrEqualTo(const MPInteger &rhs) const
    {
        return lessThan(rhs) || equalTo(rhs);
    }
    bool operator<=(const MPInteger &rhs) const
    {
        return lessThanOrEqualTo(rhs);
    }

    // operator>
    bool greaterThan(const MPInteger &rhs) const
    {
        return !lessThanOrEqualTo(rhs);
    }
    bool operator>(const MPInteger &rhs) const
    {
        return greaterThan(rhs);
    }

    // operator>=
    bool greaterThanOrEqualTo(const MPInteger &rhs) const
    {
        return !lessThan(rhs);
    }
    bool operator>=(const MPInteger &rhs) const
    {
        return greaterThanOrEqualTo(rhs);
    }
    // end of comparison operators
    //////////////////////////////

    ///////////////////////
    // assignment operators
    // operator+=
    MPInteger &additionAssignment(const MPInteger &rhs);
    MPInteger &operator+=(const MPInteger &rhs)
    {
        return additionAssignment(rhs);
    }

    // operator-=
    MPInteger &subtractionAssignment(const MPInteger &rhs);
    MPInteger &operator-=(const MPInteger &rhs)
    {
        return subtractionAssignment(rhs);
    }

    // operator*=
    MPInteger &multiplicationAssignment(const MPInteger &rhs);
    MPInteger &operator*=(const MPInteger &rhs)
    {
        return multiplicationAssignment(rhs);
    }

    // operator/=
    MPInteger &divisionAssignment(const MPInteger &rhs);
    MPInteger &operator/=(const MPInteger &rhs)
    {
        return divisionAssignment(rhs);
    }

    // operator%=
    MPInteger &moduloAssignment(const MPInteger &rhs);
    MPInteger &operator%=(const MPInteger &rhs)
    {
        return moduloAssignment(rhs);
    }

    // operator<<=
    MPInteger &bitwiseLeftShiftAssignment(const unsigned long long n);
    MPInteger &operator<<=(const unsigned long long n)
    {
        return bitwiseLeftShiftAssignment(n);
    }

    // operator>>=
    MPInteger &bitwiseRightShiftAssignment(const unsigned long long n);
    MPInteger &operator>>=(const unsigned long long n)
    {
        return bitwiseRightShiftAssignment(n);
    }
    // end of assignment operators
    //////////////////////////////

    ///////////////////////
    // arithmetic operators
    // operator+
    MPInteger addition(const MPInteger &rhs) const
    {
        MPInteger tmp(*this);
        tmp.additionAssignment(rhs);
        return tmp;
    }
    MPInteger operator+(const MPInteger &rhs) const
    {
        return addition(rhs);
    }

    // operator-
    MPInteger subtraction(const MPInteger &rhs) const
    {
        MPInteger tmp(*this);
        tmp.subtractionAssignment(rhs);
        return tmp;
    }
    MPInteger operator-(const MPInteger &rhs) const
    {
        return subtraction(rhs);
    }

    // operator*
    MPInteger multiplication(const MPInteger &rhs) const;
    MPInteger operator*(const MPInteger &rhs) const
    {
        return multiplication(rhs);
    }

    // operator/
    MPInteger division(const MPInteger &rhs) const
    {
        MPInteger tmp(*this);
        tmp.divisionAssignment(rhs);
        return tmp;
    }
    MPInteger operator/(const MPInteger &rhs) const
    {
        return division(rhs);
    }

    // operator%
    MPInteger modulo(const MPInteger &rhs) const
    {
        MPInteger tmp(*this);
        tmp.moduloAssignment(rhs);
        return tmp;
    }
    MPInteger operator%(const MPInteger &rhs) const
    {
        return modulo(rhs);
    }

    // operator<<
    MPInteger bitwiseLeftShift(const unsigned long long n) const
    {
        MPInteger tmp(*this);
        tmp.bitwiseLeftShiftAssignment(n);
        return tmp;
    }
    MPInteger operator<<(const unsigned long long n) const
    {
        return bitwiseLeftShift(n);
    }

    // operator>>
    MPInteger bitwiseRightShift(const unsigned long long n) const
    {
        MPInteger tmp(*this);
        tmp.bitwiseRightShiftAssignment(n);
        return tmp;
    }
    MPInteger operator>>(const unsigned long long n) const
    {
        return bitwiseRightShift(n);
    }
    // end of arithmetic operators
    //////////////////////////////

    ////////////////////////////////
    // increment/decrement operators
    // prefix operator++
    MPInteger &preIncrement();
    MPInteger &operator++()
    {
        return preIncrement();
    }

    // prefix operator--
    MPInteger &preDecrement();
    MPInteger &operator--()
    {
        return preDecrement();
    }

    // postfix operator++
    MPInteger postIncrement()
    {
        MPInteger tmp(*this);
        preIncrement();
        return tmp;
    }
    MPInteger operator++(int)
    {
        return postIncrement();
    }

    // postfix operator--
    MPInteger postDecrement()
    {
        MPInteger tmp(*this);
        preDecrement();
        return tmp;
    }
    MPInteger operator--(int)
    {
        return postDecrement();
    }
    // end of increment/decrement operators
    ///////////////////////////////////////

    void clear()
    {
        if (initialized_) {
            mem_.clear();
            bits_ = 0;
            initialized_ = false;
            isNegative_ = false;
        }
    }

    std::vector<ElemType> &getMem_()
    {
        return mem_;
    }

    void setMem_(const std::vector<ElemType> &m)
    {
        mem_ = m;
    }

    unsigned long long getBits_() const
    {
        return bits_;
    }

    void setBits_(unsigned long long b)
    {
        bits_ = b;
    }

    bool isNegative() const
    {
        return isNegative_;
    }

    void toNegative() const
    {
        // if the absolute value is not equal to zero
        if (initialized_ && bits_) {
            isNegative_ = true;
        }
    }

    void toNotNegative() const
    {
        if (initialized_) {
            isNegative_ = false;
        }
    }

    void toOppositeSign() const
    {
        if (initialized_) {
            if (isNegative_) {
                isNegative_ = false;
            } else if (bits_) {
                isNegative_ = true;
            }
        }
    }

    void toZero()
    {
        initialized_ = true;
        mem_.clear();
        bits_ = 0;
        isNegative_ = false;
    }

    // unary plus
    // operator+
    MPInteger operator+() const
    {
        return *this;
    }

    // unary minus
    // operator-
    MPInteger operator-() const
    {
        MPInteger tmp(*this);
        tmp.toOppositeSign();
        return tmp;
    }

    // abs(lhs) < abs(rhs): -1
    // abs(lhs) == abs(rhs): 0
    // abs(lhs) > abs(rhs): 1
    // error: others
    int absCompare(const MPInteger &rhs) const;

private:
    std::vector<ElemType> mem_;
    unsigned long long bits_;
    bool initialized_;

    // the sign is mutable
    mutable bool isNegative_;

    /////////////////////////

    static MPInteger mul(const MPInteger &a, const MPInteger &b);
    static MPInteger mul_karatsuba(const MPInteger &a, const MPInteger &b);
    static const MPInteger &getTen();
};

#endif  // _MPINTEGER_H
