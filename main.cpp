#include "MPInteger.h"
#include <iostream>

int main()
{
    MPInteger a, b;
    a.parseHex("0x200fad7696cee3eb209a255fad0cc13ccb7c6e90afcb50b8c7eed3df08ecc743");
    b.parseHex("0x4d94557376f1ef77badd73847d9b5c27e508cc838626ca0514ba9a601d5e2bf4");

    MPInteger ans = a + b;
    ans.printHex();
    std::cout << std::endl;
    ans.printBin();
    std::cout << std::endl;

    std::cout << std::endl;

    ans = a - b;
    ans.printHex();
    std::cout << std::endl;
    ans.printBin();
    std::cout << std::endl;
}
