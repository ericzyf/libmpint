#include "catch.hpp"
#include "MPInteger.h"
#include <sstream>

TEST_CASE("multiplication") {
    MPInteger a, b;

    SECTION("same sign") {
        std::ostringstream oss;

        a.parseHex("0x0");
        b.parseHex("0x0");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x0");
        b.parseHex("0x1");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x1");
        b.parseHex("0x0");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x1");
        b.parseHex("0x1");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x1");
        b.parseHex("0xfee1dead");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x7c1071f3724a84f4b75d0d39");
        b.parseHex("0x1e73bd37a8e4f1ee624bbea18");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0xec20470d54408693b9c53ab2e1bdc8fcb4ccbc96eb725758") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x38e07f4025e2de93b0322239");
        b.parseHex("0x6a6fa837587fed541c283c52");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0x17a5c3675be76504625f4fd8942db0ae0997ebe1c4fc5242") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("-0x1f87edcaa2206a64d0b3383e4");
        b.parseHex("-0x4494791882af5e52797b2d66");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("0x87264a9907208eb54b62a9091ab0d2ec7cfb7fc9a741a0d8") == 0);
        oss.str("");
        a.clear();
        b.clear();
    }

    SECTION("opposite sign") {
        std::ostringstream oss;

        a.parseHex("-0x1f87edcaa2206a64d0b3383e4");
        b.parseHex("0x4494791882af5e52797b2d66");
        a.multiplication(b).printHex(oss);
        REQUIRE(oss.str().compare("-0x87264a9907208eb54b62a9091ab0d2ec7cfb7fc9a741a0d8") == 0);
        oss.str("");
        a.clear();
        b.clear();
    }
}

TEST_CASE("modulo") {
    MPInteger a, b;

    SECTION("same sign") {
        std::ostringstream oss;

        a.parseHex("0xa10ee55eb6b29517fa8f20fe3617ad7d5c6c141a421f5ee1d94ba7c41cb706cb008bfc35c5bde6a53a768fe5839fd84b9c21302fe26a770765db026a18d15");
        b.parseHex("0x4c6dc010e864bdd3149a159cb10e78e303bc81b49eb6f5ed95");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0xa10ee55eb6b29517fa8f20fe3617ad7d5c6c141a421f5ee1d94ba7c41cb706cb008bfc35c5bde6a53a768fe5839fd84b9c21302fe26a770765db026a18d15") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x502ccba4598609a9dfeaeed9d7c638cc8c6268f315699c0343b7909669e0f188e7e5c2970483395853cfeb84a68d918a05971bcc83bd9ff19dfc32448b015");
        b.parseHex("0xa666878efb8d1db7a444aff2cc66a79974076550275014ea85");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0x502ccba4598609a9dfeaeed9d7c638cc8c6268f315699c0343b7909669e0f188e7e5c2970483395853cfeb84a68d918a05971bcc83bd9ff19dfc32448b015") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0");
        b.parseHex("0xa62bdfa4afc4c0482ea711f97d7fd26fe8cb15c28b7150b120");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0xa62bdfa4afc4c0482ea711f97d7fd26fe8cb15c28b7150b120");
        b.parseHex("0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0xa62bdfa4afc4c0482ea711f97d7fd26fe8cb15c28b7150b120") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("-0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0");
        b.parseHex("-0xa62bdfa4afc4c0482ea711f97d7fd26fe8cb15c28b7150b120");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("-0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0xfee1dead");
        b.parseHex("0xfee1dead");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x0");
        b.parseHex("0xfee1dead");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x0");
        b.parseHex("0x1");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
        b.clear();
    }

    SECTION("opposite sign") {
        std::ostringstream oss;

        a.parseHex("-0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0");
        b.parseHex("0xa62bdfa4afc4c0482ea711f97d7fd26fe8cb15c28b7150b120");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("-0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0");
        b.parseHex("-0xa62bdfa4afc4c0482ea711f97d7fd26fe8cb15c28b7150b120");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("0x6d283bf7e81bb8fd242bdfbb54d90e82e50b4e2e740e5b8ccaed2560619f1f2ec98326f62e7e82e77226d93ffe87fb17c8ed7616bbf8563a654059ea20da0") == 0);
        oss.str("");
        a.clear();
        b.clear();

        a.parseHex("-0xfee1dead");
        b.parseHex("0xfee1dead");
        ((a / b) * b + (a % b)).printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1dead") == 0);
        oss.str("");
        a.clear();
        b.clear();
    }
}
