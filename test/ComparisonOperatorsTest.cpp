#include "catch.hpp"
#include "MPInteger.h"
#include <cstdint>

TEST_CASE("equalTo") {
    MPInteger a, b;

    SECTION("corner cases") {
        a.clear();
        b.clear();
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));

        uint64_t foo = 0xfee1dead;
        a.assign(&foo, 64);
        REQUIRE(a.equalTo(a));
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));

        b.assign(&foo, 64);
        b.toNegative();
        REQUIRE(b.equalTo(b));
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));

        foo = ~foo;
        b.assign(&foo, 64);
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));
    }

    SECTION("positive numbers") {
        uint64_t m, n;

        m = n = 1;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();

        m = n = 2;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();

        m = n = 4;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();

        m = n = 64;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();

        m = 128;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));
        a.clear();
        b.clear();

        m = n = UINT64_MAX;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();
    }

    SECTION("negative numbers") {
        uint64_t m, n;

        m = n = 1;
        a.assign(&m, 64);
        a.toNegative();
        b.assign(&n, 64);
        b.toNegative();
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();

        m = n = UINT64_MAX;
        a.assign(&m, 64);
        a.toNegative();
        b.assign(&n, 64);
        b.toNegative();
        REQUIRE(a.equalTo(b));
        REQUIRE(b.equalTo(a));
        a.clear();
        b.clear();

        m = 2;
        a.assign(&m, 64);
        a.toNegative();
        b.assign(&n, 64);
        b.toNegative();
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));
        a.clear();
        b.clear();
    }

    SECTION("opposite sign") {
        uint64_t m, n;

        m = n = 1;
        a.assign(&m, 64);
        b.assign(&n, 64);
        b.toNegative();
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));

        m = 2;
        a.assign(&m, 64);
        REQUIRE(!a.equalTo(b));
        REQUIRE(!b.equalTo(a));

        a.clear();
        b.clear();
    }
}

TEST_CASE("notEqualTo") {
    MPInteger a, b;
    uint64_t m, n;

    m = n = 1;
    a.assign(&m, 64);
    b.assign(&n, 64);
    REQUIRE(!a.notEqualTo(b));
    REQUIRE(!b.notEqualTo(a));

    a.toNegative();
    REQUIRE(a.notEqualTo(b));
    REQUIRE(b.notEqualTo(a));

    n = UINT64_MAX;
    b.assign(&n, 64);
    REQUIRE(a.notEqualTo(b));
    REQUIRE(b.notEqualTo(a));
    a.clear();
    b.clear();
}

TEST_CASE("lessThan") {
    MPInteger a, b;
    uint64_t m, n;

    SECTION("corner cases") {
        REQUIRE(!a.lessThan(b));
        REQUIRE(!b.lessThan(a));
    }

    SECTION("positive numbers") {
        for (uint64_t i = 1; i <= 100; ++i) {
            m = i - 1;
            n = i;
            a.assign(&m, 64);
            b.assign(&n, 64);
            REQUIRE(a.lessThan(b));
            REQUIRE(!b.lessThan(a));
            a.clear();
            b.clear();
        }

        n = 100;
        for (m = 0; m < n; ++m) {
            a.assign(&m, 64);
            b.assign(&n, 64);
            REQUIRE(a.lessThan(b));
            REQUIRE(!b.lessThan(a));
            a.clear();
            b.clear();
        }

        a.assign(&m, 64);
        b.assign(&m, 64);
        REQUIRE(!a.lessThan(b));
        REQUIRE(!b.lessThan(a));
        a.clear();
        b.clear();
    }

    SECTION("negative numbers") {
        for (uint64_t i = 1; i <= 100; ++i) {
            m = i - 1;
            n = i;
            a.assign(&m, 64);
            a.toNegative();
            b.assign(&n, 64);
            b.toNegative();
            REQUIRE(!a.lessThan(b));
            REQUIRE(b.lessThan(a));
            a.clear();
            b.clear();
        }
    }

    SECTION("opposite sign") {
        for (uint64_t i = 1; i <= 100; ++i) {
            m = i - 1;
            n = i;
            a.assign(&m, 64);
            b.assign(&n, 64);
            b.toNegative();
            REQUIRE(!a.lessThan(b));
            REQUIRE(b.lessThan(a));
            a.clear();
            b.clear();
        }
    }
}
