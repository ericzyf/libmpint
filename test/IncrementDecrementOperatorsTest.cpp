#include "catch.hpp"
#include "MPInteger.h"
#include <sstream>

TEST_CASE("preIncrement") {
    MPInteger mpi;

    SECTION("negative") {
        std::ostringstream oss;

        mpi.parseHex("-0x2");
        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0x1") == 0);
        REQUIRE(mpi.countBits_() == 1);
        oss.str("");

        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        REQUIRE(mpi.countBits_() == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("positive") {
        std::ostringstream oss;

        mpi.parseHex("0x1");
        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x2") == 0);
        REQUIRE(mpi.countBits_() == 2);
        oss.str("");

        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x3") == 0);
        REQUIRE(mpi.countBits_() == 2);
        oss.str("");

        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x4") == 0);
        REQUIRE(mpi.countBits_() == 3);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xfffffffffffff");
        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x10000000000000") == 0);
        REQUIRE(mpi.countBits_() == 53);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1fffffffffffffffffffffffffffffffff");
        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x2000000000000000000000000000000000") == 0);
        REQUIRE(mpi.countBits_() == 134);
        oss.str("");
        mpi.clear();
    }

    SECTION("zero") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.preIncrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        REQUIRE(mpi.countBits_() == 1);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("preDecrement") {
    MPInteger mpi;

    SECTION("negative") {
        std::ostringstream oss;

        mpi.parseHex("-0xfee1dead");
        mpi.preDecrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1deae") == 0);
        REQUIRE(mpi.countBits_() == 32);
        oss.str("");
        mpi.clear();
    }

    SECTION("positive") {
        std::ostringstream oss;

        mpi.parseHex("0x3");
        mpi.preDecrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x2") == 0);
        REQUIRE(mpi.countBits_() == 2);
        oss.str("");

        mpi.preDecrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        REQUIRE(mpi.countBits_() == 1);
        oss.str("");

        mpi.preDecrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        REQUIRE(mpi.countBits_() == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xf10000000000000000000000000000000");
        mpi.preDecrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xf0fffffffffffffffffffffffffffffff") == 0);
        REQUIRE(mpi.countBits_() == 132);
        oss.str("");
        mpi.clear();
    }

    SECTION("zero") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.preDecrement();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0x1") == 0);
        REQUIRE(mpi.countBits_() == 1);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("postIncrement") {
    MPInteger mpi;

    SECTION("negative") {
        std::ostringstream oss;

        mpi.parseHex("-0xfee1dead");
        mpi.postIncrement().printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1dead") == 0);
        oss.str("");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1deac") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("positive") {
        std::ostringstream oss;

        mpi.parseHex("0xfee1dead");
        mpi.postIncrement().printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1deae") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("zero") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.postIncrement().printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("postDecrement") {
    MPInteger mpi;

    SECTION("negative") {
        std::ostringstream oss;

        mpi.parseHex("-0xfee1dead");
        mpi.postDecrement().printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1dead") == 0);
        oss.str("");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1deae") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("positive") {
        std::ostringstream oss;

        mpi.parseHex("0xfee1dead");
        mpi.postDecrement().printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1deac") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("zero") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.postDecrement().printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0x1") == 0);
        oss.str("");
        mpi.clear();
    }
}
