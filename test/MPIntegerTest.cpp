#include "catch.hpp"
#include "MPInteger.h"
#include <cstdint>
#include <iostream>
#include <sstream>

TEST_CASE("isHexadecimalString") {
    SECTION("unsigned") {
        REQUIRE(MPInteger::isHexadecimalString("") == false);
        REQUIRE(MPInteger::isHexadecimalString("0") == false);
        REQUIRE(MPInteger::isHexadecimalString("0x") == false);
        REQUIRE(MPInteger::isHexadecimalString("00x") == false);
        REQUIRE(MPInteger::isHexadecimalString("0b") == false);
        REQUIRE(MPInteger::isHexadecimalString("0xx") == false);
        REQUIRE(MPInteger::isHexadecimalString("0xg") == false);
        REQUIRE(MPInteger::isHexadecimalString("0xG") == false);
        REQUIRE(MPInteger::isHexadecimalString("0xabcdefg") == false);
        REQUIRE(MPInteger::isHexadecimalString("0X") == false);
        REQUIRE(MPInteger::isHexadecimalString("0X0123") == false);
        REQUIRE(MPInteger::isHexadecimalString("0Xffff") == false);
        REQUIRE(MPInteger::isHexadecimalString("x") == false);
        REQUIRE(MPInteger::isHexadecimalString("X") == false);
        REQUIRE(MPInteger::isHexadecimalString("x0123") == false);
        REQUIRE(MPInteger::isHexadecimalString("abcd") == false);

        REQUIRE(MPInteger::isHexadecimalString("0x0") == true);
        REQUIRE(MPInteger::isHexadecimalString("0x00") == true);
        REQUIRE(MPInteger::isHexadecimalString("0x0192740971") == true);
        REQUIRE(MPInteger::isHexadecimalString("0xabcdef0123456789") == true);
        REQUIRE(MPInteger::isHexadecimalString("0x0123456789abcdef") == true);
        REQUIRE(MPInteger::isHexadecimalString("0x0123456789aBcDeF") == true);
        REQUIRE(MPInteger::isHexadecimalString("0x0123456789ABCDEF") == true);
        REQUIRE(MPInteger::isHexadecimalString("0xABCDEF") == true);
        REQUIRE(MPInteger::isHexadecimalString("0xabcdef") == true);
        REQUIRE(MPInteger::isHexadecimalString("0xFFFFFF") == true);
        REQUIRE(MPInteger::isHexadecimalString("0x000000") == true);
    }

    SECTION("signed") {
        REQUIRE(MPInteger::isHexadecimalString("+0") == false);
        REQUIRE(MPInteger::isHexadecimalString("-0") == false);
        REQUIRE(MPInteger::isHexadecimalString("+0x") == false);
        REQUIRE(MPInteger::isHexadecimalString("-0x") == false);
        REQUIRE(MPInteger::isHexadecimalString("+0X") == false);
        REQUIRE(MPInteger::isHexadecimalString("-0X") == false);
        REQUIRE(MPInteger::isHexadecimalString("++0x0000") == false);
        REQUIRE(MPInteger::isHexadecimalString("+-0xabcdef") == false);
        REQUIRE(MPInteger::isHexadecimalString("--0xABCDEF") == false);
        REQUIRE(MPInteger::isHexadecimalString("-+0xaaaaaa") == false);
        REQUIRE(MPInteger::isHexadecimalString("+0xABCDEFG") == false);
        REQUIRE(MPInteger::isHexadecimalString("+0X0123456789") == false);
        REQUIRE(MPInteger::isHexadecimalString("+00xabcdef") == false);

        REQUIRE(MPInteger::isHexadecimalString("+0x0") == true);
        REQUIRE(MPInteger::isHexadecimalString("-0x00") == true);
        REQUIRE(MPInteger::isHexadecimalString("-0x0192740971") == true);
        REQUIRE(MPInteger::isHexadecimalString("+0xabcdef0123456789") == true);
        REQUIRE(MPInteger::isHexadecimalString("-0x0123456789abcdef") == true);
        REQUIRE(MPInteger::isHexadecimalString("+0x0123456789aBcDeF") == true);
        REQUIRE(MPInteger::isHexadecimalString("+0x0123456789ABCDEF") == true);
        REQUIRE(MPInteger::isHexadecimalString("+0xABCDEF") == true);
        REQUIRE(MPInteger::isHexadecimalString("-0xabcdef") == true);
        REQUIRE(MPInteger::isHexadecimalString("-0xFFFFFF") == true);
        REQUIRE(MPInteger::isHexadecimalString("-0x000000") == true);
    }
}

TEST_CASE("isBinaryString") {
    SECTION("unsigned") {
        REQUIRE(MPInteger::isBinaryString("") == false);
        REQUIRE(MPInteger::isBinaryString("0b") == false);
        REQUIRE(MPInteger::isBinaryString("0B") == false);
        REQUIRE(MPInteger::isBinaryString("00") == false);
        REQUIRE(MPInteger::isBinaryString("0b012") == false);
        REQUIRE(MPInteger::isBinaryString("0babcd") == false);
        REQUIRE(MPInteger::isBinaryString("0B010101") == false);
        REQUIRE(MPInteger::isBinaryString("0") == false);

        REQUIRE(MPInteger::isBinaryString("0b0") == true);
        REQUIRE(MPInteger::isBinaryString("0b1") == true);
        REQUIRE(MPInteger::isBinaryString("0b00000") == true);
        REQUIRE(MPInteger::isBinaryString("0b11111") == true);
        REQUIRE(MPInteger::isBinaryString("0b0101010010101110101") == true);
        REQUIRE(MPInteger::isBinaryString("0b1101011101010010100") == true);
    }

    SECTION("signed") {
        REQUIRE(MPInteger::isBinaryString("+") == false);
        REQUIRE(MPInteger::isBinaryString("-") == false);
        REQUIRE(MPInteger::isBinaryString("+0") == false);
        REQUIRE(MPInteger::isBinaryString("-0") == false);
        REQUIRE(MPInteger::isBinaryString("+00") == false);
        REQUIRE(MPInteger::isBinaryString("+00b") == false);
        REQUIRE(MPInteger::isBinaryString("+0b12") == false);
        REQUIRE(MPInteger::isBinaryString("+00b101010001") == false);
        REQUIRE(MPInteger::isBinaryString("+0B010111001") == false);
        REQUIRE(MPInteger::isBinaryString("++0b01011001") == false);
        REQUIRE(MPInteger::isBinaryString("--0b01011001") == false);

        REQUIRE(MPInteger::isBinaryString("+0b0000") == true);
        REQUIRE(MPInteger::isBinaryString("-0b0000") == true);
        REQUIRE(MPInteger::isBinaryString("+0b010010011101") == true);
        REQUIRE(MPInteger::isBinaryString("-0b11001010110") == true);
    }
}

TEST_CASE("isDecimalString") {
    SECTION("unsigned") {
        REQUIRE(MPInteger::isDecimalString("") == false);
        REQUIRE(MPInteger::isDecimalString("0x") == false);
        REQUIRE(MPInteger::isDecimalString("0X") == false);
        REQUIRE(MPInteger::isDecimalString("0b") == false);
        REQUIRE(MPInteger::isDecimalString("0B") == false);
        REQUIRE(MPInteger::isDecimalString("01928094710924x") == false);
        REQUIRE(MPInteger::isDecimalString("abcdef") == false);
        REQUIRE(MPInteger::isDecimalString("x908210948023") == false);

        REQUIRE(MPInteger::isDecimalString("180924863") == true);
        REQUIRE(MPInteger::isDecimalString("0") == true);
        REQUIRE(MPInteger::isDecimalString("00000000") == true);
        REQUIRE(MPInteger::isDecimalString("99999999") == true);
        REQUIRE(MPInteger::isDecimalString("98237809572901") == true);
    }

    SECTION("signed") {
        REQUIRE(MPInteger::isDecimalString("+0x") == false);
        REQUIRE(MPInteger::isDecimalString("-0x") == false);
        REQUIRE(MPInteger::isDecimalString("++00") == false);
        REQUIRE(MPInteger::isDecimalString("--00") == false);
        REQUIRE(MPInteger::isDecimalString("+++8392798") == false);
        REQUIRE(MPInteger::isDecimalString("x+00") == false);
        REQUIRE(MPInteger::isDecimalString("x-000") == false);
        REQUIRE(MPInteger::isDecimalString("+983698546389437x") == false);

        REQUIRE(MPInteger::isDecimalString("+0") == true);
        REQUIRE(MPInteger::isDecimalString("-0") == true);
        REQUIRE(MPInteger::isDecimalString("+00000") == true);
        REQUIRE(MPInteger::isDecimalString("-00000") == true);
        REQUIRE(MPInteger::isDecimalString("+0970932759823") == true);
        REQUIRE(MPInteger::isDecimalString("-87326981849") == true);
        REQUIRE(MPInteger::isDecimalString("-8753983000") == true);
    }
}

TEST_CASE("isOctalString") {
    SECTION("unsigned") {
        REQUIRE(MPInteger::isOctalString("") == false);
        REQUIRE(MPInteger::isOctalString("0") == false);
        REQUIRE(MPInteger::isOctalString("00") == false);
        REQUIRE(MPInteger::isOctalString("0x01001110100") == false);
        REQUIRE(MPInteger::isOctalString("0b00111101011") == false);
        REQUIRE(MPInteger::isOctalString("0O0") == false);
        REQUIRE(MPInteger::isOctalString("0o") == false);
        REQUIRE(MPInteger::isOctalString("0o0007778") == false);
        REQUIRE(MPInteger::isOctalString("0o01234a") == false);
        REQUIRE(MPInteger::isOctalString("00o01234567") == false);
        REQUIRE(MPInteger::isOctalString("01234") == false);
        REQUIRE(MPInteger::isOctalString("o0o0123") == false);

        REQUIRE(MPInteger::isOctalString("0o0") == true);
        REQUIRE(MPInteger::isOctalString("0o00") == true);
        REQUIRE(MPInteger::isOctalString("0o01234567") == true);
        REQUIRE(MPInteger::isOctalString("0o77777777") == true);
    }

    SECTION("signed") {
        REQUIRE(MPInteger::isOctalString("+") == false);
        REQUIRE(MPInteger::isOctalString("-") == false);
        REQUIRE(MPInteger::isOctalString("+0o") == false);
        REQUIRE(MPInteger::isOctalString("++0o000") == false);
        REQUIRE(MPInteger::isOctalString("-+0o0") == false);
        REQUIRE(MPInteger::isOctalString("+0o0123a") == false);
        REQUIRE(MPInteger::isOctalString("+0o8888") == false);
        REQUIRE(MPInteger::isOctalString("-0o8888") == false);
        REQUIRE(MPInteger::isOctalString("+0x012300") == false);
        REQUIRE(MPInteger::isOctalString("+0b000110") == false);
        REQUIRE(MPInteger::isOctalString("-0b00000") == false);
        REQUIRE(MPInteger::isOctalString("-0O00001") == false);

        REQUIRE(MPInteger::isOctalString("+0o0") == true);
        REQUIRE(MPInteger::isOctalString("+0o0000") == true);
        REQUIRE(MPInteger::isOctalString("-0o0000") == true);
        REQUIRE(MPInteger::isOctalString("+0o7777") == true);
        REQUIRE(MPInteger::isOctalString("-0o7777") == true);
        REQUIRE(MPInteger::isOctalString("-0o01234567") == true);
    }
}

TEST_CASE("assign(src, pos, len)") {
    MPInteger mpi;

    SECTION("corner cases") {
        MPInteger a;

        mpi.assign(a, 0, 4);
        REQUIRE(mpi.countBits_() == 0);

        a.parseBin("0b11");
        mpi.assign(a, 0, 4);
        REQUIRE(mpi.countBits_() == 0);

        mpi.assign(a, 2, 0);
        REQUIRE(mpi.countBits_() == 0);

        mpi.assign(a, 2, 1);
        REQUIRE(mpi.countBits_() == 0);
        a.clear();

        a.parseBin("0b100101101");
        mpi.assign(a, 0, 10);
        REQUIRE(mpi.countBits_() == 0);

        mpi.assign(a, 1, 11);
        REQUIRE(mpi.countBits_() == 0);

        mpi.assign(a, 8, 0);
        REQUIRE(mpi.countBits_() == 0);

        a.clear();
        mpi.clear();
    }

    SECTION("normal cases") {
        std::ostringstream oss;
        MPInteger a;

        a.parseBin("0b1111101010010110000001111000001110101101101001010001100010001000111000011110000011100110101100010");

        mpi.assign(a, 0, 8);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1100010") == 0);
        oss.str("");
        mpi.clear();

        mpi.assign(a, 1, 9);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b10110001") == 0);
        oss.str("");
        mpi.clear();

        mpi.assign(a, 17, 13);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b111100000") == 0);
        oss.str("");
        mpi.clear();

        mpi.assign(a, 1, 96);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b111110101001011000000111100000111010110110100101000110001000100011100001111000001110011010110001") == 0);
        oss.str("");
        mpi.clear();

        a.clear();
    }
}

TEST_CASE("parseHex") {
    MPInteger mpi;

    SECTION("unsigned") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.printHex(oss);
        REQUIRE(mpi.countBits_() == 0);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x00");
        REQUIRE(mpi.countBits_() == 0);
        mpi.clear();

        mpi.parseHex("0x000");
        REQUIRE(mpi.countBits_() == 0);
        mpi.clear();

        mpi.parseHex("0x01");
        mpi.printHex(oss);
        REQUIRE(mpi.countBits_() == 1);
        REQUIRE(oss.str().compare("0x1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x001");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x0001");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x00001");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x0000f");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xf") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xfee1dead");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xFEE1DEAD");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x0000000fee1dead");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x00fEe1dEaD");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x101");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x101") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1001");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1001") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x10001");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x10001") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x2aefa6ed9ccbe90cfa01fc8ecd29fc97db16d05ab61ff34be5d6d1cacc80397d9c9ca3b78b8b2e40c6ae9a5086e2761c6039d721b5febf694d1087ea1946ad2f6e9dbc53478d0893d00e4ef53f65639a073dcf2a68686fbc1d72cd1b0e85a3c5fd31e9f4cf046e2caa05c3099c936a95c3793c6b868fa404bdbbce258af52570c7251f325cfe410ec49538b77c4b5495fe5d09444cab79687773effcc8649ca0c960c6bfe6d357416bd557060441a0f889e00de7cfb8ae9dc0a66c0ef4b2a1f16e4e5619d017ca8f3bd2c6a284ad95bb2bad77439f73e7d3131abf1d8648f0e624ae564222ca2c29fc535884e698c6a831197815e2d95d37ef7b3db24a825703b7ada574e726a458e404f79db6ed7c97e9e9880abf082f265b41e763435a95b570e9fed453982b9fe7339b7ed3b41c4f633921f011a91899307d38bdf175b8680f258205be42c3d825d491e9ef3b4c5785b9b1a19754f0da23a7c18ddc44e7979397f66cc964b0a081e068f7b9f3102322ea07b15f6d0ddc9868c302ac47b605d1794477556ccaa8350287ad0530fa0364252613ea4c02491919c4cfd25442e5e863ea307f839c11a4dbd29abaaf06f8baca8c16661a482823945fca54d113d0d535e45ca8fbf1337337d0ce4f7c5fa2380955eddff79f69a8f0e98fed4217e57f4cad4692e258d75f97871e3c89e8ea7ba7a56440c953d1c7e2b0f30de64feb");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x2aefa6ed9ccbe90cfa01fc8ecd29fc97db16d05ab61ff34be5d6d1cacc80397d9c9ca3b78b8b2e40c6ae9a5086e2761c6039d721b5febf694d1087ea1946ad2f6e9dbc53478d0893d00e4ef53f65639a073dcf2a68686fbc1d72cd1b0e85a3c5fd31e9f4cf046e2caa05c3099c936a95c3793c6b868fa404bdbbce258af52570c7251f325cfe410ec49538b77c4b5495fe5d09444cab79687773effcc8649ca0c960c6bfe6d357416bd557060441a0f889e00de7cfb8ae9dc0a66c0ef4b2a1f16e4e5619d017ca8f3bd2c6a284ad95bb2bad77439f73e7d3131abf1d8648f0e624ae564222ca2c29fc535884e698c6a831197815e2d95d37ef7b3db24a825703b7ada574e726a458e404f79db6ed7c97e9e9880abf082f265b41e763435a95b570e9fed453982b9fe7339b7ed3b41c4f633921f011a91899307d38bdf175b8680f258205be42c3d825d491e9ef3b4c5785b9b1a19754f0da23a7c18ddc44e7979397f66cc964b0a081e068f7b9f3102322ea07b15f6d0ddc9868c302ac47b605d1794477556ccaa8350287ad0530fa0364252613ea4c02491919c4cfd25442e5e863ea307f839c11a4dbd29abaaf06f8baca8c16661a482823945fca54d113d0d535e45ca8fbf1337337d0ce4f7c5fa2380955eddff79f69a8f0e98fed4217e57f4cad4692e258d75f97871e3c89e8ea7ba7a56440c953d1c7e2b0f30de64feb") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xbfcf8ebc014901adbf8150f108bc62f15f943ac1f4ebd0f0d6fef082f043238809aafa508722db616597e36d2f660256d2e926c93f59a94551866b5e181f471a7af76db932e5ba8ec7752cf3f091376cb4a73d55167a8a20aa4fbac8ee7e1a9be2c24619ac2d9ae13fe8bfb642eb5b9ac7c307d35a254124370a069f9604c3c67e1464c3e9dec64316a12aebba5c2b345bc2dfeef3bd34a199ddc40f7c88f862252796736225333ba9c29b8c67e7a0be682209624780fbacf0dc6beb1cf9ff7a5434e36b7c20585f11e4aaea3fe9577b84bd5850b05fd3aeb77b9333f9ede39d7a9597b480142f4850cc26afe82b8029ad1a242242ef197b2e6a203315bd3ddcd778e5f9cd59cca2e59b61b991145c46d296fdaf716e747aa57a8e5afc0cfd9532c5add8a60e202a5995e46bca2f48859524a0825188e56db00ecce59b0f9b257e96a0d3d560d6f295544b1bc0ca0627860566e761df8765fa1093ae5c6f80d095d4cdd9312f46301208f9b97afd049faa18584e5664774f9cfe5b25f61aa46a81fd806d6cc3a52dc51b629c5531c3586f7ed93b4a30f2416b1da05f0159a028b9021aa2552722a499673d4453dc6b941712d57c1e740e49614d728ac7bbaac21207ee9f422c3c80381461acd5edf5e14b6967c62ca79e04f6fc67787c7ec2ca1ad1c2d3a5dbfb65881f7a897a955983012d00d0f6ea256cc93e3e6e9ce637b5");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xbfcf8ebc014901adbf8150f108bc62f15f943ac1f4ebd0f0d6fef082f043238809aafa508722db616597e36d2f660256d2e926c93f59a94551866b5e181f471a7af76db932e5ba8ec7752cf3f091376cb4a73d55167a8a20aa4fbac8ee7e1a9be2c24619ac2d9ae13fe8bfb642eb5b9ac7c307d35a254124370a069f9604c3c67e1464c3e9dec64316a12aebba5c2b345bc2dfeef3bd34a199ddc40f7c88f862252796736225333ba9c29b8c67e7a0be682209624780fbacf0dc6beb1cf9ff7a5434e36b7c20585f11e4aaea3fe9577b84bd5850b05fd3aeb77b9333f9ede39d7a9597b480142f4850cc26afe82b8029ad1a242242ef197b2e6a203315bd3ddcd778e5f9cd59cca2e59b61b991145c46d296fdaf716e747aa57a8e5afc0cfd9532c5add8a60e202a5995e46bca2f48859524a0825188e56db00ecce59b0f9b257e96a0d3d560d6f295544b1bc0ca0627860566e761df8765fa1093ae5c6f80d095d4cdd9312f46301208f9b97afd049faa18584e5664774f9cfe5b25f61aa46a81fd806d6cc3a52dc51b629c5531c3586f7ed93b4a30f2416b1da05f0159a028b9021aa2552722a499673d4453dc6b941712d57c1e740e49614d728ac7bbaac21207ee9f422c3c80381461acd5edf5e14b6967c62ca79e04f6fc67787c7ec2ca1ad1c2d3a5dbfb65881f7a897a955983012d00d0f6ea256cc93e3e6e9ce637b5") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("signed") {
        std::ostringstream oss;

        mpi.parseHex("+0xfee1dead");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("-0xfee1dead");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1dead") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("-0x0");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("-0x00");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("-0x2aefa6ed9ccbe90cfa01fc8ecd29fc97db16d05ab61ff34be5d6d1cacc80397d9c9ca3b78b8b2e40c6ae9a5086e2761c6039d721b5febf694d1087ea1946ad2f6e9dbc53478d0893d00e4ef53f65639a073dcf2a68686fbc1d72cd1b0e85a3c5fd31e9f4cf046e2caa05c3099c936a95c3793c6b868fa404bdbbce258af52570c7251f325cfe410ec49538b77c4b5495fe5d09444cab79687773effcc8649ca0c960c6bfe6d357416bd557060441a0f889e00de7cfb8ae9dc0a66c0ef4b2a1f16e4e5619d017ca8f3bd2c6a284ad95bb2bad77439f73e7d3131abf1d8648f0e624ae564222ca2c29fc535884e698c6a831197815e2d95d37ef7b3db24a825703b7ada574e726a458e404f79db6ed7c97e9e9880abf082f265b41e763435a95b570e9fed453982b9fe7339b7ed3b41c4f633921f011a91899307d38bdf175b8680f258205be42c3d825d491e9ef3b4c5785b9b1a19754f0da23a7c18ddc44e7979397f66cc964b0a081e068f7b9f3102322ea07b15f6d0ddc9868c302ac47b605d1794477556ccaa8350287ad0530fa0364252613ea4c02491919c4cfd25442e5e863ea307f839c11a4dbd29abaaf06f8baca8c16661a482823945fca54d113d0d535e45ca8fbf1337337d0ce4f7c5fa2380955eddff79f69a8f0e98fed4217e57f4cad4692e258d75f97871e3c89e8ea7ba7a56440c953d1c7e2b0f30de64feb");
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0x2aefa6ed9ccbe90cfa01fc8ecd29fc97db16d05ab61ff34be5d6d1cacc80397d9c9ca3b78b8b2e40c6ae9a5086e2761c6039d721b5febf694d1087ea1946ad2f6e9dbc53478d0893d00e4ef53f65639a073dcf2a68686fbc1d72cd1b0e85a3c5fd31e9f4cf046e2caa05c3099c936a95c3793c6b868fa404bdbbce258af52570c7251f325cfe410ec49538b77c4b5495fe5d09444cab79687773effcc8649ca0c960c6bfe6d357416bd557060441a0f889e00de7cfb8ae9dc0a66c0ef4b2a1f16e4e5619d017ca8f3bd2c6a284ad95bb2bad77439f73e7d3131abf1d8648f0e624ae564222ca2c29fc535884e698c6a831197815e2d95d37ef7b3db24a825703b7ada574e726a458e404f79db6ed7c97e9e9880abf082f265b41e763435a95b570e9fed453982b9fe7339b7ed3b41c4f633921f011a91899307d38bdf175b8680f258205be42c3d825d491e9ef3b4c5785b9b1a19754f0da23a7c18ddc44e7979397f66cc964b0a081e068f7b9f3102322ea07b15f6d0ddc9868c302ac47b605d1794477556ccaa8350287ad0530fa0364252613ea4c02491919c4cfd25442e5e863ea307f839c11a4dbd29abaaf06f8baca8c16661a482823945fca54d113d0d535e45ca8fbf1337337d0ce4f7c5fa2380955eddff79f69a8f0e98fed4217e57f4cad4692e258d75f97871e3c89e8ea7ba7a56440c953d1c7e2b0f30de64feb") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("parseBin") {
    MPInteger mpi;

    SECTION("unsigned") {
        std::ostringstream oss;

        mpi.parseBin("0b0");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 0);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b00");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 0);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b000");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 0);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b00000000");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 0);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b000000000");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 0);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b00001");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 1);
        REQUIRE(oss.str().compare("0b1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b0000000101");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 3);
        REQUIRE(oss.str().compare("0b101") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b1001011");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 7);
        REQUIRE(oss.str().compare("0b1001011") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b100101101");
        mpi.printBin(oss);
        REQUIRE(mpi.countBits_() == 9);
        REQUIRE(oss.str().compare("0b100101101") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b1001101000101001011110001110000000010110101010001110111101111010010110000110000100011001101000100");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1001101000101001011110001110000000010110101010001110111101111010010110000110000100011001101000100") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b1100011011011100111000001010000100011011101010001101111110100011100100000110111010100010100011000");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1100011011011100111000001010000100011011101010001101111110100011100100000110111010100010100011000") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b1101010101011000011100110011000011011010111001110010111111101000110000001110100001010111010000001");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1101010101011000011100110011000011011010111001110010111111101000110000001110100001010111010000001") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b1011101101000001001110011110100101111011111101101000111000011110011101001001010110101000001111110");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1011101101000001001110011110100101111011111101101000111000011110011101001001010110101000001111110") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("0b11100001000111110111111111000000000100101001111011000000011011110101001000110111111000010100001");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b11100001000111110111111111000000000100101001111011000000011011110101001000110111111000010100001") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("signed") {
        std::ostringstream oss;

        mpi.parseBin("+0b0");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("-0b0");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("+0b1010101");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1010101") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseBin("-0b1010101");
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("-0b1010101") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("parseDec") {
    MPInteger mpi;

    SECTION("unsigned") {
        std::ostringstream oss;

        mpi.parseDec("0");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("0000");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("0123");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("123") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("99999999999999999999999999999999999999999999999999999999999999999999");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("99999999999999999999999999999999999999999999999999999999999999999999") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("009900");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("9900") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("173702500093895961809647794317430934573379402924336529145412968734475199215763109167267155364257836443126010847252802718746232191166741205148589113288244540529327666394216219809442666571735598616806501840813037530047016059970690039409840452470973429838909413819282943156809524774850273445045545207220330148062166835135750749818612298595220866536975590737941661014305819601008603491759928800727613125106516185742932623786055579418058675250512790501808961452263961926359318332124797035901309465082952621222830702972174308216928451554148980720518235969080481236005840072651771041197564853823066982625467228463130419804089");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("173702500093895961809647794317430934573379402924336529145412968734475199215763109167267155364257836443126010847252802718746232191166741205148589113288244540529327666394216219809442666571735598616806501840813037530047016059970690039409840452470973429838909413819282943156809524774850273445045545207220330148062166835135750749818612298595220866536975590737941661014305819601008603491759928800727613125106516185742932623786055579418058675250512790501808961452263961926359318332124797035901309465082952621222830702972174308216928451554148980720518235969080481236005840072651771041197564853823066982625467228463130419804089") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("signed") {
        std::ostringstream oss;

        mpi.parseDec("+0");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("-0");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("-0");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("-92375896437812083092754893246517340972045723");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("-92375896437812083092754893246517340972045723") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("+92375896437812083092754893246517340972045723");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("92375896437812083092754893246517340972045723") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseDec("-173702500093895961809647794317430934573379402924336529145412968734475199215763109167267155364257836443126010847252802718746232191166741205148589113288244540");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("-173702500093895961809647794317430934573379402924336529145412968734475199215763109167267155364257836443126010847252802718746232191166741205148589113288244540") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("parseOct") {
    MPInteger mpi;

    SECTION("unsigned") {
        std::ostringstream oss;

        mpi.parseOct("0o0");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("0o0000");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("0o01");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("0o337637552033464056022402563751567346457473443");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o337637552033464056022402563751567346457473443") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("0o1501476075441626560420363571177012732161340233376130735116");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o1501476075441626560420363571177012732161340233376130735116") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("0o264645646710705367641106233712460517307503114315062473172");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o264645646710705367641106233712460517307503114315062473172") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("signed") {
        std::ostringstream oss;

        mpi.parseOct("+0o01");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o1") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("+0o264645646710705367641106233712460517307503114315062473172");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o264645646710705367641106233712460517307503114315062473172") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseOct("-0o264645646710705367641106233712460517307503114315062473172");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("-0o264645646710705367641106233712460517307503114315062473172") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("printHex") {
    MPInteger mpi;

    SECTION("zero") {
        std::ostringstream oss;

        int foo = 0;
        mpi.assign(&foo, 8);
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        mpi.clear();
        oss.str("");
    }

    SECTION("positive numbers") {
        std::ostringstream oss;
        uint64_t u64;

        u64 = 1;
        mpi.assign(&u64, 64);
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x1") == 0);
        mpi.clear();
        oss.str("");

        u64 = 2;
        mpi.assign(&u64, 64);
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x2") == 0);
        mpi.clear();
        oss.str("");

        u64 = 0xfee1dead;
        mpi.assign(&u64, 64);
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0xfee1dead") == 0);
        mpi.clear();
        oss.str("");

        u64 = 0x0123456789abcdef;
        mpi.assign(&u64, 64);
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("0x123456789abcdef") == 0);
        mpi.clear();
        oss.str("");
    }

    SECTION("negative numbers") {
        std::ostringstream oss;
        uint64_t u64;

        u64 = 1;
        mpi.assign(&u64, 64);
        mpi.toNegative();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0x1") == 0);
        mpi.clear();
        oss.str("");

        u64 = 0xfee1dead;
        mpi.assign(&u64, 64);
        mpi.toNegative();
        mpi.printHex(oss);
        REQUIRE(oss.str().compare("-0xfee1dead") == 0);
        mpi.clear();
        oss.str("");
    }
}

TEST_CASE("printBin") {
    MPInteger mpi;

    SECTION("zero") {
        std::ostringstream oss;

        int foo = 0;
        mpi.assign(&foo, 8);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b0") == 0);
        mpi.clear();
        oss.str("");
    }

    SECTION("positive numbers") {
        std::ostringstream oss;
        uint64_t u64;

        u64 = 1;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1") == 0);
        mpi.clear();
        oss.str("");

        u64 = 2;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b10") == 0);
        mpi.clear();
        oss.str("");

        u64 = 4;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b100") == 0);
        mpi.clear();
        oss.str("");

        u64 = 7;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b111") == 0);
        mpi.clear();
        oss.str("");

        u64 = 10;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1010") == 0);
        mpi.clear();
        oss.str("");

        u64 = 0xfee1dead;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b11111110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 31);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1111110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 30);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b111110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 29);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b11110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 28);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 27);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 26);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b10111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        mpi.assign(&u64, 25);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");

        u64 = UINT64_MAX;
        mpi.assign(&u64, 64);
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("0b1111111111111111111111111111111111111111111111111111111111111111") == 0);
        mpi.clear();
        oss.str("");
    }

    SECTION("negative numbers") {
        std::ostringstream oss;
        uint64_t u64;

        u64 = 1;
        mpi.assign(&u64, 64);
        mpi.toNegative();
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("-0b1") == 0);
        mpi.clear();
        oss.str("");

        u64 = 0xfee1dead;
        mpi.assign(&u64, 64);
        mpi.toNegative();
        mpi.printBin(oss);
        REQUIRE(oss.str().compare("-0b11111110111000011101111010101101") == 0);
        mpi.clear();
        oss.str("");
    }
}

TEST_CASE("printDec") {
    MPInteger mpi;

    SECTION("zero") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("0") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("positive numbers") {
        std::ostringstream oss;

        mpi.parseHex("0x34e8d463ec151e99004e7f8a17380aa45d19e1");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("1179921073340930015655148381036644939220195809") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1fac3ca2f7ec8051e767383546c059a71c68d4781");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("2893115588194127022294658805048612639751118079873") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x5169895b68866796ffb578166");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("403133946511714748865143341414") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1d5bb86486442ed8d6dbb6eb905d04f5");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("39023850819625972462829580747536139509") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1cf0046837af4ea653ec11bbda4261dabb5869b4c");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("2643268869491880479604705019956973805689941433164") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1b07c7e3b47c9ef267792282da855257e9720c740f2468d55");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("10604533247741986403192375510118832199773648749006535691605") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x72a9071fa4fe80cb233a255d525f43899b9afc692b6731b800514f7941ac4efd00714ddeaf9d68e812eb6d8a3047b5ac5121a08374191fd75178a295064c2d628c69c75cb23554bbdf988f17c24f9f2195085d889dde0fa730bd2e68c39418c5296aa953f42d2aa7fd069482fc3f19231dd9161cb91398f85e37448570a9dc99");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("80517176225759447285348880692027374992272137780433992224625484620062732033413896586090640453320502585254273936934835691879897790149933760120298158198973857253070778964160334231591337010005886518874829934101856214186169680703711228362277413481743092519225329356583177695502225791222303064769018676147579903129") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("negative numbers") {
        std::ostringstream oss;

        mpi.parseHex("-0x72a9071fa4fe80cb233a255d525f43899b9afc692b6731b800514f7941ac4efd00714ddeaf9d68e812eb6d8a3047b5ac5121a08374191fd75178a295064c2d628c69c75cb23554bbdf988f17c24f9f2195085d889dde0fa730bd2e68c39418c5296aa953f42d2aa7fd069482fc3f19231dd9161cb91398f85e37448570a9dc99");
        mpi.printDec(oss);
        REQUIRE(oss.str().compare("-80517176225759447285348880692027374992272137780433992224625484620062732033413896586090640453320502585254273936934835691879897790149933760120298158198973857253070778964160334231591337010005886518874829934101856214186169680703711228362277413481743092519225329356583177695502225791222303064769018676147579903129") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("printOct") {
    MPInteger mpi;

    SECTION("unsigned") {
        std::ostringstream oss;

        mpi.parseHex("0x0");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o0") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x7");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o7") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x8");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o10") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xd8a98c56e8025ea92c7ae9a4a847a5669be2595e79fa5bce8");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o154251430533500045725113075351511241075126323370454536363751336350") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0xa029ec7fa0d691f1887bb1afb6425ad3ab0a58a898ba36555");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o120051730776406551076142075661537331022655165302454250461350662525") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x1c5f849669c37d405caf143803703b0cdf25c445b4d2a93468");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o342770222632341575200562570503400334035414676227042133232252232150") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("0x56f6e639a7e840e77c0a99740d214088fa4945bb187f478ab58cc6b455dfc94ee0bcda917dcab958f3d2e15e0bb938f0629913df5e6d66768e74ab3c8755e071d7613da5be16d87f3f3ed1cfd30d143f84ccbfab09e10fa72c38679b3c506899ab560b07e2d91f2ecc495c4e25520e4f323a2ac93991545e5e25fcc1b75fcff1");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("0o53366714346477204034737005231350064412010437222242673060775074253261461532125677445167013633244276712562543636456053602734470740612310475753633263166434722531710352570070727302366455741333037637476643477230321207741146277526047410372345416063633170501504632552602603742662174566304453423422522034474621642531116310524274570457714066727747761") == 0);
        oss.str("");
        mpi.clear();
    }

    SECTION("signed") {
        std::ostringstream oss;

        mpi.parseHex("-0x1c5f849669c37d405caf143803703b0cdf25c445b4d2a93468");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("-0o342770222632341575200562570503400334035414676227042133232252232150") == 0);
        oss.str("");
        mpi.clear();

        mpi.parseHex("-0x56f6e639a7e840e77c0a99740d214088fa4945bb187f478ab58cc6b455dfc94ee0bcda917dcab958f3d2e15e0bb938f0629913df5e6d66768e74ab3c8755e071d7613da5be16d87f3f3ed1cfd30d143f84ccbfab09e10fa72c38679b3c506899ab560b07e2d91f2ecc495c4e25520e4f323a2ac93991545e5e25fcc1b75fcff1");
        mpi.printOct(oss);
        REQUIRE(oss.str().compare("-0o53366714346477204034737005231350064412010437222242673060775074253261461532125677445167013633244276712562543636456053602734470740612310475753633263166434722531710352570070727302366455741333037637476643477230321207741146277526047410372345416063633170501504632552602603742662174566304453423422522034474621642531116310524274570457714066727747761") == 0);
        oss.str("");
        mpi.clear();
    }
}

TEST_CASE("countBits_") {
    MPInteger mpi;

    SECTION("construct from uint8_t") {
        uint8_t u8;

        // let mpi = 0
        u8 = 0;
        mpi.assign(&u8, 8);
        REQUIRE(mpi.countBits_() == 0);
        mpi.clear();

        for (int i = 1; i <= 7; ++i) {
            mpi.assign(&u8, i);
            REQUIRE(mpi.countBits_() == 0);
            mpi.clear();
        }

        // let mpi = 1
        u8 = 1;
        mpi.assign(&u8, 8);
        REQUIRE(mpi.countBits_() == 1);
        mpi.clear();

        // let mpi = 2
        u8 = 2;
        mpi.assign(&u8, 8);
        REQUIRE(mpi.countBits_() == 2);
        mpi.clear();

        // let mpi = 4
        u8 = 4;
        mpi.assign(&u8, 8);
        REQUIRE(mpi.countBits_() == 3);
        mpi.clear();

        // let 64 <= mpi <= 127
        for (u8 = 64; u8 <= 127; ++u8) {
            mpi.assign(&u8, 8);
            REQUIRE(mpi.countBits_() == 7);
            mpi.clear();
        }

        // let mpi >= 128
        for (int i = 128; i <= 255; ++i) {
            u8 = static_cast<uint8_t>(i);
            mpi.assign(&u8, 8);
            REQUIRE(mpi.countBits_() == 8);
            mpi.clear();
        }
    }

    SECTION("construct from int64_t") {
        int64_t i64;

        // let mpi = 0
        i64 = 0;
        mpi.assign(&i64, 64);
        REQUIRE(mpi.countBits_() == 0);
        mpi.clear();

        for (int i = 1; i <= 63; ++i) {
            mpi.assign(&i64, i);
            REQUIRE(mpi.countBits_() == 0);
            mpi.clear();
        }

        // let mpi = 1
        i64 = 1;
        mpi.assign(&i64, 64);
        REQUIRE(mpi.countBits_() == 1);
        mpi.clear();

        // let mpi = 2
        i64 = 2;
        mpi.assign(&i64, 64);
        REQUIRE(mpi.countBits_() == 2);
        mpi.clear();

        // let mpi = 1 << 42
        i64 = static_cast<int64_t>(1) << 42;
        mpi.assign(&i64, 64);
        REQUIRE(mpi.countBits_() == 43);
        mpi.clear();
        // copy lower bits of i64
        for (int i = 1; i <= 42; ++i) {
            mpi.assign(&i64, i);
            REQUIRE(mpi.countBits_() == 0);
            mpi.clear();
        }

        // let mpi = 1 << 63
        i64 = INT64_MIN;
        mpi.assign(&i64, 64);
        REQUIRE(mpi.countBits_() == 64);
        mpi.clear();

        // let mpi = 0x7fff_ffff_ffff_ffff
        i64 = INT64_MAX;
        mpi.assign(&i64, 64);
        REQUIRE(mpi.countBits_() == 63);
        mpi.clear();
    }

    SECTION("construct from uint64_t") {
        uint64_t u64;

        // let mpi = 0;
        u64 = 0;
        mpi.assign(&u64, 64);
        REQUIRE(mpi.countBits_() == 0);
        mpi.clear();

        for (int i = 1; i <= 63; ++i) {
            mpi.assign(&u64, i);
            REQUIRE(mpi.countBits_() == 0);
            mpi.clear();
        }

        // let mpi = 1;
        u64 = 1;
        mpi.assign(&u64, 64);
        REQUIRE(mpi.countBits_() == 1);
        mpi.clear();

        // let mpi = 2 ^ 64 - 1
        u64 = UINT64_MAX;
        mpi.assign(&u64, 64);
        REQUIRE(mpi.countBits_() == 64);
        mpi.clear();

        // let mpi = 2 ^ 63 - 1
        u64 = (static_cast<uint64_t>(1) << 63) - 1;
        mpi.assign(&u64, 64);
        REQUIRE(mpi.countBits_() == 63);
        mpi.clear();
    }
}

TEST_CASE("bitAt") {
    MPInteger mpi;

    SECTION("corner cases") {
        uint16_t u16 = 0x81afU;
        mpi.assign(&u16, 16);

        REQUIRE(mpi.bitAt(0) == 1);
        REQUIRE(mpi.bitAt(1) == 1);
        REQUIRE(mpi.bitAt(2) == 1);
        REQUIRE(mpi.bitAt(3) == 1);

        REQUIRE(mpi.bitAt(4) == 0);
        REQUIRE(mpi.bitAt(5) == 1);
        REQUIRE(mpi.bitAt(6) == 0);
        REQUIRE(mpi.bitAt(7) == 1);

        REQUIRE(mpi.bitAt(8) == 1);
        REQUIRE(mpi.bitAt(9) == 0);
        REQUIRE(mpi.bitAt(10) == 0);
        REQUIRE(mpi.bitAt(11) == 0);

        REQUIRE(mpi.bitAt(12) == 0);
        REQUIRE(mpi.bitAt(13) == 0);
        REQUIRE(mpi.bitAt(14) == 0);
        REQUIRE(mpi.bitAt(15) == 1);

        // corner cases
        REQUIRE(mpi.bitAt(16) == -1);
        REQUIRE(mpi.bitAt(17) == -1);
        REQUIRE(mpi.bitAt(65536) == -1);
        REQUIRE(mpi.bitAt(-1) == -1);

        mpi.clear();
    }

    SECTION("construct from uint8_t") {
        uint8_t u8;

        u8 = 0;
        mpi.assign(&u8, 8);
        for (int i = 0; i < 8; ++i) {
            REQUIRE(mpi.bitAt(i) == 0);
        }
        mpi.clear();

        u8 = 0xff;
        mpi.assign(&u8, 8);
        for (int i = 0; i < 8; ++i) {
            REQUIRE(mpi.bitAt(i) == 1);
        }
        mpi.clear();
    }

    SECTION("construct from uint64_t") {
        uint64_t u64;

        u64 = 0;
        mpi.assign(&u64, 64);
        for (int i = 0; i < 64; ++i) {
            REQUIRE(mpi.bitAt(i) == 0);
        }
        mpi.clear();

        u64 = UINT64_MAX;
        mpi.assign(&u64, 64);
        for (int i = 0; i < 64; ++i) {
            REQUIRE(mpi.bitAt(i) == 1);
        }
        mpi.clear();
    }
}

TEST_CASE("fetchBits") {
    const unsigned char p[32] = {
        0xc5, 0x43, 0x89, 0x3f, 0x32, 0xb3, 0xf2, 0x72,
        0xed, 0x6e, 0x11, 0xc7, 0x47, 0x0d, 0x5e, 0xa9,
        0x25, 0x4e, 0x0b, 0x05, 0x20, 0x0b, 0x8b, 0xfe,
        0xae, 0x7a, 0x0b, 0x36, 0x3c, 0xdb, 0xc0, 0x3e
    };

    MPInteger mpi(p, 256);

    SECTION("corner cases") {
        REQUIRE(mpi.fetchBits(-1) == 0);
        REQUIRE(mpi.fetchBits(253) == 1);
        REQUIRE(mpi.fetchBits(254) == 0);
        REQUIRE(mpi.fetchBits(255) == 0);
        REQUIRE(mpi.fetchBits(256) == 0);
        REQUIRE(mpi.fetchBits(512) == 0);
    }

    SECTION("normal cases") {
        for (unsigned long long pos = 0; pos < mpi.getBits_(); ++pos) {
            MPInteger::ElemType ans = 0;
            for (int i = std::min(mpi.getBits_() - pos, static_cast<unsigned long long>(MPInteger::ELEM_BITNUM_)), j = pos;
                 i > 0; --i, ++j) {
                ans |= static_cast<MPInteger::ElemType>(mpi.bitAt(j)) << (j - pos);
            }

            REQUIRE(mpi.fetchBits(pos) == ans);
        }
    }
}

TEST_CASE("toZero") {
    SECTION("uninitialized") {
        std::ostringstream oss;
        MPInteger a;
        a.toZero();
        a.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
    }

    SECTION("initialized") {
        std::ostringstream oss;
        MPInteger a;
        a.parseHex("-0xfee1dead");

        a.toZero();
        a.printHex(oss);
        REQUIRE(oss.str().compare("0x0") == 0);
        oss.str("");
        a.clear();
    }
}

TEST_CASE("absCompare") {
    MPInteger a, b;

    SECTION("corner cases") {
        REQUIRE(a.absCompare(b) != -1);
        REQUIRE(a.absCompare(b) != 0);
        REQUIRE(a.absCompare(b) != 1);

        REQUIRE(b.absCompare(a) != -1);
        REQUIRE(b.absCompare(a) != 0);
        REQUIRE(b.absCompare(a) != 1);

        REQUIRE(a.absCompare(a) != -1);
        REQUIRE(a.absCompare(a) != 0);
        REQUIRE(a.absCompare(a) != 1);

        uint32_t v = 114514;
        a.assign(&v, 32);
        REQUIRE(a.absCompare(a) == 0);
    }

    SECTION("same sign") {
        uint64_t m, n;

        m = n = 0x01234567fee1dead;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.absCompare(b) == 0);
        REQUIRE(b.absCompare(a) == 0);
        a.clear();
        b.clear();

        --n;
        a.assign(&m, 64);
        b.assign(&n, 64);
        REQUIRE(a.absCompare(b) == 1);
        REQUIRE(b.absCompare(a) == -1);

        a.toOppositeSign();
        b.toOppositeSign();
        REQUIRE(a.absCompare(b) == 1);
        REQUIRE(b.absCompare(a) == -1);
        a.clear();
        b.clear();

        for (n = 1; n <= 100; ++n) {
            m = n - 1;
            a.assign(&m, 64);
            b.assign(&n, 64);
            REQUIRE(a.absCompare(b) == -1);
            REQUIRE(b.absCompare(a) == 1);
            a.clear();
            b.clear();
        }
    }

    SECTION("opposite sign") {
        uint64_t m, n;

        m = n = 0xfee1dead76543210U;
        a.assign(&m, 64);
        b.assign(&n, 64);
        b.toOppositeSign();
        REQUIRE(a.absCompare(b) == 0);
        REQUIRE(b.absCompare(a) == 0);
        a.clear();
        b.clear();

         --n;
        a.assign(&m, 64);
        b.assign(&n, 64);
        a.toOppositeSign();
        REQUIRE(a.absCompare(b) == 1);
        REQUIRE(b.absCompare(a) == -1);

        a.toOppositeSign();
        b.toOppositeSign();
        REQUIRE(a.absCompare(b) == 1);
        REQUIRE(b.absCompare(a) == -1);
        a.clear();
        b.clear();
    }
}
